package com.lingdian.util;

import java.util.UUID;

public class UUIDGenerator {
	
	public static String UUID(){
		return UUID.randomUUID().toString().replace("-", "");
	}

}
