package com.lingdian.crm.system;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.lingdian.crm.system.controller.MenuController;

public class SysConfig extends JFinalConfig{

	@Override
	public void configConstant(Constants me) {
		loadPropertyFile("a_little_config.txt");
		me.setDevMode(true);
	}

	@Override
	public void configRoute(Routes me) {

		me.add("menu", MenuController.class);
	}

	@Override
	public void configPlugin(Plugins me) {
		// 配置C3p0数据库连接池插件
	  C3p0Plugin c3p0Plugin = new C3p0Plugin(getProperty("jdbcUrl"), getProperty("user"), getProperty("password").trim());
	  me.add(c3p0Plugin);
				
				// 配置ActiveRecord插件
	  ActiveRecordPlugin arp = new ActiveRecordPlugin(c3p0Plugin);
	  me.add(arp);
	  arp.addMapping("Menu", Menu.class);	// 映射blog 表到 Blog模型
		
	}

	@Override
	public void configInterceptor(Interceptors me) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void configHandler(Handlers me) {
		// TODO Auto-generated method stub
		
	}

}
