package com.lingdian.crm.system.controller;

import java.util.List;





import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.kit.JsonKit;
import com.lingdian.crm.system.Menu;
import com.lingdian.util.UUIDGenerator;

public class MenuController extends Controller{

	public void listMenu(){
		List<Menu> menus = Menu.dao.findAll();
		String s2 = JSON.toJSONString(menus);
		System.out.println(s2);
		String s = JsonKit.toJson(menus, 8);
		System.out.println(s);
		
		renderJson(menus);
	}
	public void saveOrupdateMenu(){
		Menu menu = getModel(Menu.class);
		boolean success=false;
		if(menu.getStr("pid").equals("0")){
			menu.set("pid", null);
		}
		if(menu.get("id")==null){
			menu.set("id",UUIDGenerator.UUID());
		   success = menu.save();
		}else{
			menu.update();
		}
	
		renderJson(success);
	}
	
	public void updateMenu(){
		String title = getPara("title");
		String url = getPara("url");
		String id = getPara("id");
		boolean success  = Menu.dao.findById(id).set("title",title).set("url",url).update();
		renderJson(success);
	}
	public void deleteMenu(){
		boolean success  = Menu.dao.deleteById(getPara("id"));
		renderJson(success);
	}
}
