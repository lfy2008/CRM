package com.lingdian.crm.system;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
/**
 +-------+--------------+------+-----+---------+-------+
| Field | Type         | Null | Key | Default | Extra |
+-------+--------------+------+-----+---------+-------+
| id    | varchar(255) | NO   | PRI | NULL    |       |
| title | varchar(255) | YES  |     | NULL    |       |
| url   | varchar(255) | YES  |     | NULL    |       |
| pid   | varchar(255) | YES  | MUL | NULL    |       |
 * @author dell
 *
 */
public class Menu extends Model<Menu>{

	public static final Menu dao = new Menu();
	
	private List<Menu> children;
	public List<Menu> findAll(){
		List<Menu> menus = dao.find("select * from menu where pid is null");
		for(Menu menu:menus){
			List<Menu> childList = dao.find("select * from menu where pid=?", menu.getStr("id"));
			menu.setChildren(childList);
		}
		return menus;
	}
	public List<Menu> getChildren() {
		return children;
	}
	public void setChildren(List<Menu> children) {
		this.children = children;
	}
	
}
