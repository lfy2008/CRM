<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="commons.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>菜单管理</title>
	 
</head>
<script type="text/javascript">

</script>
<body>
<table id="tt" ></table>

	<div id="editDiaLog" class="easyui-dialog" title="添加菜单" style="width:400px" closed="true">
		<div style="padding:10px 60px 20px 60px">
	    <form id="ff" method="post">
	    	<table cellpadding="5">
	    		<tr>
	    			<td>菜单名称:</td>
	    			<td><input class="easyui-validatebox textbox" type="text" name="menu.title" data-options="required:true" id="title"></input></td>
	    		</tr>
	    		<tr>
	    			<td>URL:</td>
	    			<td><input class="easyui-validatebox textbox" type="text" name="menu.url" data-options="required:true" id="url"></td>
	    		</tr>
	    		<input type="hidden" name="menu.pid" id="parentId"/>
	    		<input type="hidden" name="menu.id" id="menuId"/>
	    	</table>
	    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="subForm()">确定</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" >取消</a>
	    </form>
	    </div>
	</div>
	<div id="bar">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="addForm()" data-options="iconCls:'icon-add'">新增</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="editForm()" data-options="iconCls:'icon-edit'">修改</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="deleteForm()" data-options="iconCls:'icon-cancel'">删除</a>
	</div>
</body>
	<script>
	var randomNu = (new Date().getTime()) ^ Math.random();
		$('#tt').treegrid({
		    url:'${ctx}/menu/listMenu?='+randomNu,
		    idField:'id',
		    treeField:'title',
		    toolbar:'#bar',
		    columns:[[
		        {title:'名称',field:'title',width:200},
		        {field:'url',title:'url',width:400}
		    ]]
		});
		function addForm(){
			var pnode = $("#tt").treegrid('getSelected');
			if(pnode==null){
				$("#parentId").val(0);//顶级菜单
			}else{
				$("#parentId").val(pnode.id);
			}
			$("#title").val("");
			$("#url").val("");
			$("#menuId").val("");
			$("#saveOrupdate").val("");
			$("#editDiaLog").dialog('open');
		}
		function subForm(){
			var url ="${ctx}/menu/saveOrupdateMenu";
			$("#ff").form({
				url:url,
				method:'post',
				dataType:'json',
				contentType: "application/json; charset=utf-8",
				onSubmit:function(){
					console.log("validate!");
					return true;
				},
				success:function(data){
					if(data==true){
						
					}else{
						
					}
					$("#editDiaLog").dialog('close');
					$('#tt').treegrid('reload');
				}
			});
			$('#ff').submit();
		}
		function deleteForm(){
			var menu = $("#tt").treegrid("getSelected");
			$.ajax({
				url:'${ctx}/menu/deleteMenu?id='+menu.id,
				dateType:'json',
				contentType: "application/json; charset=utf-8",
				success:function(data){
					if(data=='success'){
						
					}else{
						
					}
					$('#tt').treegrid('reload');
				}
			});
		}
		function editForm(){
			var menu = $("#tt").treegrid("getSelected");
			$("#title").val(menu.title);
			$("#url").val(menu.url);
			$("#menuId").val(menu.id);
			$("#parentId").val(menu._parentId);
			$("#saveOrupdate").val("update");
			$("#editDiaLog").dialog('open');
			
		}
	</script>
</html>