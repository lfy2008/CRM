<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="commons.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body>
<body class="easyui-layout">
    <div data-options="region:'north',title:'',split:true" style="height:100px;"></div>
    <div data-options="region:'east',title:'East',split:true" style="width:100px;"></div>
    <div data-options="region:'west',title:'菜单',href:'${ctx}/crm/menu.jsp'" style="width:210px;">
    </div>
    <div data-options="region:'center',title:''" style="padding:5px;background:#eee;">
    	<div id="tt" class="easyui-tabs" data-options="fit:true" >
			<div title="Home">
			</div>
		</div>
    
    </div>
</body>
 
</body>
<script type="text/javascript">
function addTab(title, url){
	url="${ctx}/"+url;
	if ($('#tt').tabs('exists', title)){
		$('#tt').tabs('select', title);
	} else {
		var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
		$('#tt').tabs('add',{
			title:title,
			content:content,
			closable:true
		});
	}
}
</script>
</html>