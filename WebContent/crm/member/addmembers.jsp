<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
	<link rel="stylesheet" type="text/css" href="../ext/easyui/themes/default/easyui.css">
	<link rel="stylesheet" type="text/css" href="../ext/easyui/themes/icon.css">
	<script type="text/javascript" src="../ext/easyui/jquery.min.js"></script>
	<script type="text/javascript" src="../ext/easyui/jquery.easyui.min.js"></script>
</head>
<body>
<div class="easyui-panel" title="添加会员" style="width:600px;">
		<div style="padding:10px 60px 20px 60px">
	    <form id="ff" method="post" action="member/add">
	    	<table cellpadding="5">
	    		<tr>
	    			<td>登录名:</td>
	    			<td><input class="easyui-validatebox textbox" type="text" name="loginName" data-options="required:true"></input></td>
	    		</tr>
	    		<tr>
	    			<td>昵称:</td>
	    			<td><input class="easyui-validatebox textbox" type="text" name="nickName" data-options="required:true"></input></td>
	    		</tr>
	    		<tr>
	    			<td>密码:</td>
	    			<td><input class="easyui-validatebox password" type="password" name="password" data-options="required:true"></input></td>
	    		</tr>
	    	</table>
	    </form>
	    <div style="text-align:center;padding:5px">
	    	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="submitForm()">提交</a>
	    	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()">取消</a>
	    </div>
	    </div>
	</div>
</body>
	<script>
	function showAddForm(){
		$("#ff").form("open");
	}
		function submitForm(){
			$('#ff').form('submit');
		}
		function clearForm(){
			$('#ff').form('clear');
		}
	</script>
</html>