<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="commons.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Insert title here</title>
</head>
<body><!-- 
<a href="javascript:void(0)" onclick="addTab('自定义回复','${ctx}/crm/configmessage.jsp')">自定义回复</a>

 -->
<ul id="tmenu"></ul>

<script type="text/javascript">
$('#tmenu').tree({
    url:'${ctx}/crm/system/menus',
    formatter:function(node){
		return node.title;
	},
	onClick:function(node){
		addTab(node.title,node.url);
	}
});
</script> 
</body>
</html>