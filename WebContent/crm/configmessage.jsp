<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="commons.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>微信菜单管理</title>
	 
</head>
<script type="text/javascript">

</script>
<body>
<div  id="tb">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="addForm()" data-options="iconCls:'icon-add'">新增</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="editForm()" data-options="iconCls:'icon-edit'">修改</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="deleteForm()" data-options="iconCls:'icon-cancel'">删除</a>
</div>
<table id="dg"   class="easyui-datagrid" title="微信自定义回复"
			data-options="
			rownumbers:true,
			singleSelect:true,
			pagination:true,
			toolbar:'#tb',
			fit:'true',
			idFiedl:'id',
			url:'${ctx}/crm/config/message/list',method:'get'">
		<thead>
			<tr>
				<!-- <th data-options="field:'id',fit:true">ID</th> -->
				<th data-options="field:'code',fit:true">指令</th>
				<th data-options="field:'message',width:100">回复内容</th>
				<th data-options="
				field:'status',
				width:100,
				formatter:function(value,row,index){
					if(value=='Y'){
						return '启用';
					}else{
						return '关闭'					
					}
				}
				">状态</th>
			</tr>
		</thead>
	</table>
	<div id="editDiaLog" class="easyui-dialog" title="添加指令" style="width:400px" closed="true">
		<div style="padding:10px 60px 20px 60px">
	    <form id="ff" method="post">
	    	<table cellpadding="5">
	    		<tr>
	    			<td>指令:</td>
	    			<td><input class="easyui-validatebox textbox" type="text" name="code" data-options="required:true"></input></td>
	    		</tr>
	    		<tr>
	    			<td>内容:</td>
	    			
	    			<td><textarea name="message"></textarea></td>
	    		</tr>
	    	</table>
	    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="subForm()">确定</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" >取消</a>
	    </form>
	    </div>
	</div>
</body>
	<script>
	function addForm(){
		$("#editDiaLog").dialog('open');
	}
	function subForm(){
		$("#ff").form({
			url:'${ctx}/crm/config/message/save',
			method:'post',
			onSubmit:function(){
				console.log("validate!");
				return true;
			},
			success:function(data){
				if(data=="success"){
					
				}else{
					
				}
				$("#editDiaLog").dialog('close');
				$('#dg').datagrid('reload');
			}
		});
		$('#ff').submit();
	}
		
	</script>
</html>